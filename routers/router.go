package routers

import (
	"github.com/astaxie/beego"
	"gitlab.aseit.cn/AI_PRODUCT/MachineInfoRegister/controllers"
)

// 提供三个路由请求的api
func init() {
	beego.Router("/", &controllers.MachineController{})
	beego.Router("/getMachineList", &controllers.MachineController{}, "get:GetMachineList")
	beego.Router("/register", &controllers.MachineController{})  // 解析到重写的post方法
}

