package controllers

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/astaxie/beego"
	"gitlab.aseit.cn/AI_PRODUCT/MachineInfoRegister/tools"
)

type request struct {
	UserName string   `json:"userName"`
	Ip       string   `json:"ip"`
	Mac      []string `json:"mac"`
}

type regResponse struct {
	ErrCode      int      `json:"errCode"`
	ErrMsg       string   `json:"errMsg"`
	RegisterTime string   `json:"registerTime"`
	Mac          []string `json:"mac"`
}

type getMachineResponse struct {
	Machines []machineInfo `json:"machines"`
}

type machineInfo struct {
	Ip           string   `json:"ip"`
	UserName     string   `json:"userName"`
	Mac          []string `json:"mac"`
	RegisterTime string   `json:"registerTime"`
}

var mapMachineInfo = make(map[string]*machineInfo)
var rwmutexMachine sync.RWMutex
var mutexFile sync.Mutex

type MachineController struct {
	beego.Controller
}

// Get 返回注册ip username time 的列表,html模板渲染
func (c *MachineController) Get() {
	rwmutexMachine.RLock()
	tmpMapMachineInfo := mapMachineInfo
	rwmutexMachine.RUnlock()
	c.Data["content"] = tmpMapMachineInfo // MachineInfo map
	c.TplName = "index.tpl"
}

// writeToFile 创建date路径，把ip、username、registertime写入对应的日志文件
func (c *MachineController) writeToFile(machine *machineInfo) {
	mutexFile.Lock()
	defer mutexFile.Unlock()
	tools.CreateDir("./" + tools.GenDatePath())
	t := time.Now()
	f, err := os.OpenFile("./"+tools.GenDatePath()+"/"+t.Format("2006-01-02")+".log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err.Error())
	}
	// []string 转 string
	temp, err := json.Marshal(machine.Mac)
	if err != nil {
		panic(err.Error())
	}
	Mac := string(temp)
	msg := machine.Ip + " " + machine.UserName + " " + machine.RegisterTime + " " + Mac + "\n"
	_, err = f.Write([]byte(msg))
	if err != nil {
		fmt.Println(err.Error())
	}
	f.Close()
}

// Post 重写，构造request和response，接收保存请求的ip，username 和 registertime信息，接收post方法的register请求
func (c *MachineController) Post() {
	req := request{}
	rep := regResponse{}
	t := time.Now()
	rep.RegisterTime = t.Format("2006-01-02 15:04:05")
	defer c.ServeJSON()
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &req); err != nil {
		rep.ErrCode = -1
		rep.ErrMsg = err.Error()
		c.Data["json"] = rep
		return
	}
	fmt.Println(req.Ip, req.UserName, len(req.Ip), req.Mac)
	if req.Ip == "" || req.UserName == "" {
		rep.ErrCode = -2
		rep.ErrMsg = "ip or userName must be not null"
		c.Data["json"] = rep
		return
	}
	var machine machineInfo
	machine.Ip = req.Ip
	machine.UserName = req.UserName
	machine.RegisterTime = rep.RegisterTime
	machine.Mac = req.Mac
	rwmutexMachine.Lock()
	mapMachineInfo[req.Ip] = &machine
	rwmutexMachine.Unlock()
	c.writeToFile(&machine)
	rep.ErrMsg = "success"
	c.Data["json"] = rep
}

// GetMachineList 返回json数据，包含现有的所有注册机器信息
func (c *MachineController) GetMachineList() {
	rep := getMachineResponse{}
	rwmutexMachine.RLock()
	for _, machine := range mapMachineInfo {
		var machine machineInfo = *machine
		rep.Machines = append(rep.Machines, machine)
	}
	rwmutexMachine.RUnlock()
	c.Data["json"] = rep
	c.ServeJSON()
}

// 使客户端开机自启，并定时请求注册
// 服务端校验username存在，应更新ip信息而非添加新数据
