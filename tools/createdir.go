package tools

import (
	// "fmt"
	"log"
	"os"
	"time"
)

// CreateDir ...
func CreateDir(Path string) {
	log.Println("make direction:", Path)
	err := os.Mkdir(Path, 0777)
	err2 := os.Chmod(Path, 0777)
	if err != nil && err2 != nil {
		log.Printf("Error: %s failed to make a direction!", err)
	}
}

// GenDatePath get date string
func GenDatePath() string {
	Date := time.Now().Format("2006-01-02")
	return Date
}

// func main() {
// 	date := GenDatePath()
// 	fmt.Println(date)
// 	CreateDir("./" + date)
// }
