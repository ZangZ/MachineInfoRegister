.PHONY: all server client clean

all:server client

server: server/main.go
	go build -o bin/$@ $^

client: client/main.go
	go build -o bin/$@ $^

clean:
	rm -rf bin