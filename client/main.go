package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/user"
	"strconv"
	"strings"
	"time"
)

var ip string
var tick *time.Ticker

type request struct {
	UserName string   `json:"userName"`
	Ip       string   `json:"ip"`
	Mac      []string `json:"mac"`
}

func init() {
	flag.StringVar(&ip, "i", "", "the register server ip,like 192.168.5.48:8085")
}

func convert(b []byte) string {
	s := make([]string, len(b))
	for i := range b {
		s[i] = strconv.Itoa(int(b[i]))
	}
	return strings.Join(s, ".")
}

func getIpFromAddr(addr net.Addr) net.IP {
	var ip net.IP
	switch v := addr.(type) {
	case *net.IPNet:
		ip = v.IP
	case *net.IPAddr:
		ip = v.IP
	}
	if ip == nil || ip.IsLoopback() {
		return nil
	}
	ip = ip.To4()
	if ip == nil {
		return nil // not an ipv4 address
	}

	return ip
}

func getIp() string {
	ifaces, _ := net.Interfaces()
	// handle err
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			ip := getIpFromAddr(addr)
			if ip == nil {
				continue
			}
			return convert(ip)
		}
		// process IP address
	}
	return ""
}

func getMac() []string {
	var macs []string
	interfaces, err := net.Interfaces()
	if err != nil {
		panic("Error:" + err.Error())
	}

	for _, inter := range interfaces {
		mac := inter.HardwareAddr.String() // 获取本机MAC地址
		if mac == "" {
			continue
		}
		fmt.Println("Mac:", mac)
		macs = append(macs, mac)

	}

	return macs
}

// // readProcFile 抽取读取硬件信息的函数
// func readProcFile(filePath string) ([]byte, error) {
// 	return ioutil.ReadFile(filePath)
// }

// // getCpu 获取cpu 型号 核心数 和 温度
// func getCpu() map[string]string {

// 	var cpuInfo map[string]string
// 	cpuInfo = make(map[string]string)

// 	cpuInfo["cpuModelName"] = "i5"
// 	return cpuInfo
// }

func register() {
	var req request
	u, _ := user.Current()
	req.Ip = getIp()
	req.Mac = getMac()
	req.UserName = u.Username
	reqMsg, _ := json.Marshal(req)
	_, err := http.Post("http://"+ip+"/register", "application/x-www-form-urlencoded", strings.NewReader(string(reqMsg)))
	fmt.Println(strings.NewReader(string(reqMsg)))
	if err != nil {
		fmt.Println("Post error ", "http://"+ip+"/register", string(reqMsg))
	}
}

func main() {
	flag.Parse()
	if ip == "" {
		fmt.Println("must specified the server ip,like 192.168.5.48:8085")
		os.Exit(-1)
	}
	register()
	tick = time.NewTicker(3600 * time.Second)
	for {
		<-tick.C
		register()
	}
}
